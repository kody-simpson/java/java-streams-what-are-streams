package me.kodysimpson;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        //What in tarnation is a stream?
        //The best illustration that exists for streams is that they are like a pipeline.
        //The pipeline begins with a source(like a Collection)
        //Intermediate operations - things that are done to the elements going through the pipe, returns a new Stream when done
        //Terminal operations - operations that end the stream and return the final output

        //https://www.bing.com/images/search?view=detailV2&ccid=OMIDxcik&id=6CF1FD8861926D817A8AEC16EF00DE4552DD624C&thid=OIP.OMIDxcikFxVZAPQp3FHLGgHaC9&mediaurl=https%3a%2f%2fwww.bytestree.com%2fwp-content%2fuploads%2f2018%2f02%2fStream-Operation.png&exph=320&expw=800&q=stream+api+terminate+operations&simid=608033139125783856&ck=B6AE7A7491AB71F3A5855AA3AE539812&selectedIndex=2&FORM=IRPRST&ajaxhist=0
        //https://www.bing.com/images/search?view=detailV2&ccid=%2FRw2Wq1Z&id=EE1F5DB9CD14F4788E11527B504AFEADBA0017EF&thid=OIP._Rw2Wq1ZDJLgPZhHbJM7YgHaHc&mediaurl=https%3A%2F%2Fcdn.britannica.com%2F96%2F151196-050-EBE18780%2FAutomobile-assembly-line.jpg&exph=1600&expw=1593&q=assembly+line&simid=608022698060350422&ck=C9168AA74C74463C52EC512A989E81E0&selectedindex=6&form=IRPRST&ajaxhist=0&vt=0&sim=11

        //our stream source
        HashSet<Integer> numbers = new HashSet<>();
        numbers.add(2);
        numbers.add(5);
        numbers.add(17);
        numbers.add(50);

        //Other ways to obtain a stream
        //Arrays.stream()
        //Stream.of()

        //start a new stream pipeline
        Set<String> newNumbers = numbers.stream()
                .filter(integer -> integer > 5) //filter numbers greater than 5
                .map(String::valueOf) //convert the stream to a Stream of Strings
                .collect(Collectors.toSet()); //terminal operation - Produce a resulting Set

        System.out.println(newNumbers);



    }
}
